<?php


namespace app\controllers;

use app\models\Settings;
use app\models\User;
use Yii;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function actionSendTicket()
    {
        Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
        Yii::$app->response->headers->set('Accept-Language', '*');
        Yii::$app->response->headers->set('Access-Control-Allow-Credentials', true);
        Yii::$app->response->headers->set('Access-Control-Request-Headers', ['X-ACCESS_TOKEN', 'Access-Control-Allow-Origin', 'Authorization', 'Origin', 'x-requested-with', 'Content-Type', 'Content-Range', 'Content-Disposition', 'Content-Description']);
        Yii::$app->response->headers->set('Access-Control-Request-Method', ['GET', 'OPTIONS']);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->request->isGet || Yii::$app->request->get('agent')) {
            $url = Yii::$app->request->get('image');
            $img = 'uploads/' . time() . '.png';
            $ch = curl_init($url);
            $fp = fopen($img, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);

            $agent = \Yii::$app->request->get('agent');
            $phone = $this->clearPhone(\Yii::$app->request->get('phone'));
            $phone = $agent ? $phone : '998' . $phone;
            $event = \Yii::$app->request->get('event');
            $image = 'https://telegram.cultureticket.uz/' . $img; //
            $orderId = \Yii::$app->request->get('orderId');
            $fullName = \Yii::$app->request->get('fullName');
            $date = \Yii::$app->request->get('date');
            $time = \Yii::$app->request->get('time');
            $marketplace = \Yii::$app->request->get('marketPlace');
            $address = \Yii::$app->request->get('address');
            $sector = \Yii::$app->request->get('sector');
            $row = \Yii::$app->request->get('row');
            $place = \Yii::$app->request->get('place');
            $isMuseum = \Yii::$app->request->get('hasSeatInfo')  === "true";

            $user = User::find()->where(['phone' => $phone])->one();
            if($user){
                $message = "<b>🎫 Ваш билет №" . $orderId . "</b>

- Мероприятия: <b>" . $event . "</b>
- ФИО: <b>" . $fullName . "</b>
- Дата: <b>" . $date . "</b>
- Время: <b>" . $time . "</b>
- Место проведения: <b>" . $marketplace . "</b>
- Адрес: <b>" . $address . "</b>";
                if (!$isMuseum) {

                    $message = $message . "
<code>-------------</code>
- Сектор: <b>" . $sector . "</b>
- Ряд: <b>" . $row . "</b>
- Место: <b>" . $place . "</b>";
                }
                if ($image) {
                    $this->sendPhoto($image, $user->chat_id, $message);
                } else {
                    $this->sendMessage($user->chat_id, $message);
                }
                return [
                    'success' => true,
                    'message' => 'Сообщение отправлено'
                ];
            }

            Yii::$app->response->statusCode = 422;
            return [
                'success' => false,
                'message' => 'Такой пользователь не найден. Подпишитесь, пожалуйста на бота в Telegram'
            ];
        }
        // Yii::$app->response->statusCode = 404;
        return [
            'success' => false,
            'message' => 'Не правильный запрос'
        ];
    }

    /**
     * @param $urlImage
     * @param $chatID
     * @param $message
     * @return bool|string
     */
    public function sendPhoto($urlImage, $chatID, $message)
    {
        $token = $this->getToken();
        $url = "https://api.telegram.org/bot" . $token . "/sendPhoto?chat_id=" . $chatID . "&parse_mode=HTML";
        $post_fields = [
            'chat_id' => $chatID,
            'photo' => $urlImage,
            'caption' => $message
        ];

        $ch = curl_init();

        $optArray = array(
            CURLOPT_HTTPHEADER => ["Content-Type:multipart/form-data"],
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $post_fields
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @param $chatID
     * @param $messaggio
     * @return bool|string
     */
    public function sendMessage($chatID, $messaggio)
    {
        $token = $this->getToken();
        $url = "https://api.telegram.org/bot" . $token . "/sendMessage?chat_id=" . $chatID . '&parse_mode=HTML';
        $url = $url . "&text=" . urlencode($messaggio);
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        $setting = Settings::findOne(AuxiliaryController::SETTING_DEFAULT_ID);
        return $setting->bot_token;
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function clearPhone($phone) {
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace("-", "", $phone);
        $phone = str_replace("+", "", $phone);
        $phone = str_replace("(", "", $phone);
        $phone = str_replace(")", "", $phone);

        return $phone;
    }

}