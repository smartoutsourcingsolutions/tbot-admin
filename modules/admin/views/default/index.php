<!-- Start Page Content -->
<div class="row">
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= number_format($amount,0," "," ") ?></h2>
                    <p class="m-b-0">Сумма заказов (сум)</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?=  $ordersCount ?></h2>
                    <p class="m-b-0">Количество заказов</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-archive f-s-40 color-warning"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $productsCount ?></h2>
                    <p class="m-b-0">Количество продуктов</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $usersCount ?></h2>
                    <p class="m-b-0">Количество килентов</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Последные заказы </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Номер заказа</th>
                            <th>Имя</th>
                            <th>Телефон номер</th>
                            <th>Username</th>
                            <th>Сумма заказа</th>
                            <th>Время заказа</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $item):?>
                        <?php $user = \app\models\User::findOne(['chat_id' => $item->user_id]); ?>
                        <tr>
                            <td><a href="<?= \yii\helpers\Url::to(['/admin/orders/view','id' => $item->id]) ?>"><?= $item->id ?></a> </td>
                            <td><?= $user->name . ' ' . $user->lastname  ?></td>
                            <td>+<?= $item->phone ?></td>
                            <td><?= $user->username ? '<a href="https://t.me/ '.$user->username .'">'. $user->username.'</a>' : '' ?></td>
                            <td><?= number_format($item->amount,0," "," ")?> Сум</td>
                            <td><?= date('d.m.Y H:i',$item->created_at)?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End PAge Content -->