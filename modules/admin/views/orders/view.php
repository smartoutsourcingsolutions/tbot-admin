<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1>Номер заказа № <?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            [
                'attribute' => 'Name',
                'value' => function($model) {
                    return $model->user ? $model->user->name . ' ' .$model->user->lastname : '';
                }
            ],
            [
                'attribute' => 'Name',
                'value' => function($model) {
                    return $model->user ? $model->user->name : '';
                }
            ],
            'phone',
            'address',
            'amount',
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return date('d.m.Y H:i', $model->created_at);
                }
            ],
        ],
    ]) ?>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-title">
                    <h4>Заказанные продукты</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Product ID</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Count</th>
                                <th>Amount</th>
                                <th>Category</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($model->orderProducts as $products): ?>
                                <tr>
                                    <td><?= $products->id; ?></td>
                                    <td><?= isset($products->product->name_ru) ? $products->product->name_ru : ''; ?></td>
                                    <td><?= number_format($products->price, '0',' ',' '); ?> сум</td>
                                    <td><?= $products->count; ?></td>
                                    <td><?= number_format($products->amount, '0',' ',' '); ?> сум</td>
                                    <td><?= isset($products->product->category->name_ru) ? $products->product->category->name_ru : ' '; ?></td>
                                    <td>
                                        <a href="<?= \yii\helpers\Url::to(['/admin/products/view', 'id' => $products->product_id]); ?>">
                                            <i class="fa fa-eye"></i></a>
                                    </td>

                                </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($model->lat)):?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <input id="lat" class="hidden" value="<?= $model->lat; ?>" />
                        <input id="lng" class="hidden" value="<?= $model->lng; ?>" />
                        <div class="mapsColl">

                            <div class="field" id="map3" style="height: 300px; width: 100% ">
                                <script>



                                    function initMap( ) {
                                        var lat1 = Number(document.getElementById("lat").value);
                                        var lng1 = Number(document.getElementById("lng").value);
                                        var uluru = {lat: lat1, lng: lng1};
                                        var map = new google.maps.Map(document.getElementById('map3'), {
                                            zoom: 15,
                                            center: uluru

                                        });
                                        var marker = new google.maps.Marker({

                                            position: uluru,
                                            map: map
                                        });
                                    }


                                </script>
                                <script async defer
                                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                                </script>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>
