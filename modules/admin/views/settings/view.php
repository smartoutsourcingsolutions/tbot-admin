<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-view">

    <h1>Настройка бота</h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bot_token',
            'channel_id',
            'send_feedback_to_admin',
            'start_text:ntext',
            'start_ru_text:ntext',
            'start_uz_text:ntext',
            'main_text_ru:ntext',
            'main_text_uz:ntext',
            'feedback_text_ru:ntext',
            'feedback_text_uz:ntext',
            'from_feedback_to_main_ru:ntext',
            'from_feedback_to_main_uz:ntext',
            'categories_ru:ntext',
            'categories_uz:ntext',
            'cart_uz:ntext',
            'cart_ru:ntext',
            'enter_address_ru:ntext',
            'enter_address_uz:ntext',
            'enter_phone_ru:ntext',
            'enter_phone_uz:ntext',
            'confirm_order_ru:ntext',
            'confirm_order_uz:ntext',
            'thanks_to_order_ru:ntext',
            'thanks_to_order_uz:ntext',

        ],
    ]) ?>

</div>
