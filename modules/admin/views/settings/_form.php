<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bot_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start_text')->textarea(['rows' => '6']) ?>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'channel_id')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'start_ru_text')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'main_text_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'feedback_text_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'from_feedback_to_main_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'categories_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'cart_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'enter_address_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'enter_phone_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'confirm_order_ru')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'thanks_to_order_ru')->textarea(['rows' => '6']) ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'send_feedback_to_admin')->dropDownList([app\models\BaseModel::STATUS_ACTIVE => Yii::t('app', 'Yes'), app\models\BaseModel::STATUS_INACTIVE => Yii::t('app', 'Not')]) ?>

            <?= $form->field($model, 'start_uz_text')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'main_text_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'feedback_text_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'from_feedback_to_main_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'categories_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'cart_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'enter_address_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'enter_phone_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'confirm_order_uz')->textarea(['rows' => '6']) ?>
            <?= $form->field($model, 'thanks_to_order_uz')->textarea(['rows' => '6']) ?>

        </div>

    </div>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
