<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Settings'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'bot_token',
            'channel_id',
            'start_text:ntext',
            'start_ru_text:ntext',
            //'start_uz_text:ntext',
            //'main_text_ru:ntext',
            //'main_text_uz:ntext',
            //'feedback_text_ru:ntext',
            //'feedback_text_uz:ntext',
            //'from_feedback_to_main_ru:ntext',
            //'from_feedback_to_main_uz:ntext',
            //'categories_ru:ntext',
            //'categories_uz:ntext',
            //'cart_uz:ntext',
            //'cart_ru:ntext',
            //'enter_address_ru:ntext',
            //'enter_address_uz:ntext',
            //'enter_phone_ru:ntext',
            //'enter_phone_uz:ntext',
            //'confirm_order_ru:ntext',
            //'confirm_order_uz:ntext',
            //'thanks_to_order_ru:ntext',
            //'thanks_to_order_uz:ntext',
            //'send_feedback_to_admin',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-eye']) . ' View', $url, ['class' => 'btn btn-success']);
                    },
                    'update' => function($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-pencil']) . ' Update', $url, ['class' => 'btn btn-primary']);
                    },
                    'delete' => function($url, $model) {
                        return Html::a(Html::tag('i', '', ['class' => 'mdi mdi-delete']) . ' Delete', $url, [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ]
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
