<?php

namespace app\modules\admin\controllers;

use app\models\Orders;
use app\models\Products;
use app\models\User;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $orders = Orders::find()
            ->limit(15)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
        $usersCount = User::find()->all();
        $ordersCount = Orders::find()->all();
        $productsCount = Products::find()->all();
        $amount = Orders::find()->sum('amount');
        return $this->render('index',[
            'orders' => $orders,
            'usersCount' => count($usersCount),
            'ordersCount' => count($ordersCount),
            'productsCount' => count($productsCount),
            'amount' => $amount
        ]);
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}
