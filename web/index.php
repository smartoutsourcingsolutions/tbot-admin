<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, X-Token-Auth, Crm-Auth, Authorization, X-Csrf-Token');
header('Access-Control-Allow-Credentials: true');

error_reporting(E_ALL);

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

// Environment
require(__DIR__ . '/../config/bootstrap.php');
require(__DIR__ . '/../config/helpers.php');

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
