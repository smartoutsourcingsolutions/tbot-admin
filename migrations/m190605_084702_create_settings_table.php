<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190605_084702_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'bot_token' => $this->string(),
            'start_text' => $this->text(),
            'start_ru_text' => $this->text(),
            'start_uz_text' => $this->text(),
            'feedback_text_ru' => $this->text(),
            'feedback_text_uz' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
