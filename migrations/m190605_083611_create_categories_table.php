<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%categories}}`
 */
class m190605_083611_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name_ru' => $this->string()->notNull(),
            'name_uz' => $this->string(),
            'icon' => $this->string(),
            'description_ru' => $this->text(),
            'description_uz' => $this->text(),
            'image' => $this->string(),
            'status' => $this->smallInteger(),
            'order' => $this->smallInteger(),
            'parent_id' => $this->integer(),
        ]);

        // creates index for column `parent_id`
        $this->createIndex(
            '{{%idx-categories-parent_id}}',
            '{{%categories}}',
            'parent_id'
        );

        // add foreign key for table `{{%categories}}`
        $this->addForeignKey(
            '{{%fk-categories-parent_id}}',
            '{{%categories}}',
            'parent_id',
            '{{%categories}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%categories}}`
        $this->dropForeignKey(
            '{{%fk-categories-parent_id}}',
            '{{%categories}}'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            '{{%idx-categories-parent_id}}',
            '{{%categories}}'
        );

        $this->dropTable('{{%categories}}');
    }
}
