<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%orders}}`
 * - `{{%products}}`
 */
class m190605_084236_create_order_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_products}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count' => $this->integer(),
            'price' => $this->integer(),
            'amount' => $this->integer(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-order_products-order_id}}',
            '{{%order_products}}',
            'order_id'
        );

        // add foreign key for table `{{%orders}}`
        $this->addForeignKey(
            '{{%fk-order_products-order_id}}',
            '{{%order_products}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-order_products-product_id}}',
            '{{%order_products}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-order_products-product_id}}',
            '{{%order_products}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%orders}}`
        $this->dropForeignKey(
            '{{%fk-order_products-order_id}}',
            '{{%order_products}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-order_products-order_id}}',
            '{{%order_products}}'
        );

        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-order_products-product_id}}',
            '{{%order_products}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-order_products-product_id}}',
            '{{%order_products}}'
        );

        $this->dropTable('{{%order_products}}');
    }
}
