<?php

use yii\db\Migration;
use mdm\admin\components\Configs;

class m000000_000001_create_user extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $userTable = Configs::instance()->userTable;
        $db = Configs::userDb();

        // Check if the table exists
        if ($db->schema->getTableSchema($userTable, true) === null) {
            $this->createTable($userTable, [
                'id' => $this->primaryKey(),
                'user_id' => $this->string(),
                'username' => $this->string(32),
                'name' => $this->string(),
                'lastname' => $this->string(),
                'phone' => $this->string(),
                'photo' => $this->string(),
                'language' => $this->string(),
                'back' => $this->string(),
                'auth_key' => $this->string(32),
                'password_hash' => $this->string(),
                'password_reset_token' => $this->string(),
                'status' => $this->smallInteger()->defaultValue(10),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                ], $tableOptions);
        }
    }

    public function down()
    {
        $userTable = Configs::instance()->userTable;
        $db = Configs::userDb();
        if ($db->schema->getTableSchema($userTable, true) !== null) {
            $this->dropTable($userTable);
        }
    }
}
