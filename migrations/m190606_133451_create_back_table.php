<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%back}}`.
 */
class m190606_133451_create_back_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%back}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'methods' => $this->string(),
            'status' => $this->smallInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%back}}');
    }
}
